const express = require("express");
const router = express.Router();
const LessonsController = require('../controllers/lesson-controller');

const prefix = '/lesson';

router.get('/', LessonsController.index);
router.post('/',async (req, res) => {
    return await LessonsController.save(req, res)
});
router.get('/:id',  LessonsController.getById);
router.put('/:id',  LessonsController.update);
router.delete('/:id',  LessonsController.delete);

module.exports = {
    prefix,
    router
};

