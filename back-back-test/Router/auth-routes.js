const express = require("express");
const AuthController = require("../controllers/auth-controller");
const router = express.Router();

const prefix = "/api/auth";

router.post("/signin", AuthController.signin);
router.post("/create", AuthController.create);

module.exports = {
    prefix: prefix,
    router: router
};
