const mongoose = require("mongoose");

const LessonModel = new mongoose.Schema({
    title: {type: String, required : true},
    content: {type: Object, required: false},
    createdAt : {type: Date, required : true, default :new Date()},
    updateAt : {type: Date, required : true, default :new Date()},
    userId : {type : String, required : true},
    status : {type : String, required: true}

})

module.exports = mongoose.model('Lesson', LessonModel)
