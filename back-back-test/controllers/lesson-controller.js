const LessonModel = require('../models/lesson-model');

class LessonsController {

    index(req, res) {
        LessonModel.find((err, lesson) => {
            res.send(lesson);
        });
    }

    async save(req, res) {
        const lesson = new LessonModel({
            title: req.body.title,
            content: req.body.content,
            createdAt: req.body.createdAt,
            updatedAt: req.body.updatedAt,
            userId: req.body.userId,
            status: req.body.status
        });
        await lesson.save();
        return res.status(201).send({message: "Lesson created successfully!"});
    }

    async getById(req, res) {
        const id = req.params.id;
        await LessonModel.findById(id);
        return res.status(200).send(lesson);
    }


    update(req, res) {

        const id = req.params.id;
        LessonModel.findByIdAndUpdate(id, req.body, {new: true}, (err, lesson) => {
            if (!!err) {
                res.status(404).send({message: 'Lesson not found!'});
            }
            res.status(200).send({message: "Lesson updated successfully!", lesson});

        });
    }

    delete(req, res) {
        const id = req.params.id;

        LessonModel.findByIdAndDelete(id, (err, lesson) => {
            if (!!err) {
                res.status(404).send({message: "Lesson not found!"});
            }
            res.send({message: 'Lesson deleted successfully!', lesson});
        })
    }

}

module.exports = new LessonsController();
