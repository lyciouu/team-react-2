import React from "react";
import "../../Assets/Styles/styles.css";
import LessonContent from "./content-component";
import Header from "./Header-component/header-component";


const CreationPage = () => {

    return (
        <div className="background-grey">
            <Header />
            <LessonContent />
        </div>
    )
}

export default CreationPage;
