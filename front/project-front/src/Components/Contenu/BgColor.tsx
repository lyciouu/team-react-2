import React,{useState} from 'react';
import './Stylescontenu.css';

export const BgColor =()=>{
    const [hex,setHex]=useState("#ffffff");
    const randomizedHex=()=>{
        const randomColor="#"+Math.floor(Math.random()*16777215).toString(16);
        setHex(randomColor);
    };
    return(
        <div className="BgColor">
            <button onClick={randomizedHex}>Randomized</button>

        </div>
    )

}
export default BgColor;