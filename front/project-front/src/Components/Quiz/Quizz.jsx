import React, { useState} from "react";
import './quiz.css';
import ButtonSave from '../Buttons/button-save-component';

export const Quiz = () => {

    const [data, setData] = useState({
        title: "",
        question: "",

    });
    const [inputList, setInputList] = useState([{response: "", checkbox: false}]);


    const handleChange = (e) => {
        const {value, name} = e.target;
        setData({
            ...data,
            [name]: value,
        });
    };

    const handleInputChange = (e, index, check) => {
        const {name, value} = e.target;
        const list = [...inputList];
        list[index][name] = check !== undefined ? check : value;
        setInputList(list);
    };

    const handleRemoveClick = (index) => {
        const list = [...inputList];
        list.splice(index, 1);
        setInputList(list);
    };

    const handleAddClick = () => {
        setInputList([...inputList, {response: "", checkbox: false}]);
    };

    const onSubmitForm = (event) => {
        event.preventDefault();
        const {title, question,} = event.target;
        setData({
            title: title.value,
            question: question.value,
        });
        event.preventDefault();
        const {response, checkbox} = event.target;
        setInputList([{
            response: response.value,
            checkbox: checkbox.value
        }])

        const qcm = {data, inputList}
        console.log(qcm)
    }
    return (
        <>
        <div className="container px-0">
            <h1 className="title-blue d-flex justify-content-center">Quiz</h1>
            <form className=" row px-0 " onSubmit={(e) => onSubmitForm(e)}>
                <input type="text"
                       name="title"
                       className=" col-6 offset-3 form-control inputs-zones my-2"
                       placeholder="Titre ..."
                       value={data.title}
                       onChange={(e) => handleChange(e)}
                       required={false}
                />
                <input type="text"
                       name="question"
                       className=" col-6 offset-3 form-control inputs-zones mt-1 mb-4"
                       placeholder="Question ..."
                       onChange={(e) => handleChange(e)}
                       value={data.question}
                       required={true}
                />

                <div className="container col-12">
                    {inputList.map((x, i) => {
                        return (
                            <div key={i} className="row px-0 ">
                                <input
                                    type="text"
                                    name="response"
                                    className=" col-6 offset-3 form-control inputs-zones "
                                    placeholder={"Réponse " + (i + 1) + "..."}
                                    onChange={(e) => handleInputChange(e, i)}
                                    value={x.response}
                                />
                                {inputList.length !== 1 &&
                                <button className="btn btn min"
                                        onClick={() => handleRemoveClick(i)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                         className="bi bi-dash-circle-fill" viewBox="0 0 16 16">
                                        <path
                                            d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h7a.5.5 0 0 0 0-1h-7z" fill="black"/>
                                    </svg>
                                </button>
                                }

                                <div className=" row col-6 offset-3  form-control-lg  mt-1">
                                    <input type="checkbox"
                                           name="checkbox"
                                           onChange={(e) => handleInputChange(e, i, e.target.checked)}
                                           checked={x.checkbox}
                                           className=" custom-control-input"
                                           id={i}


                                    />
                                    <label className="custom-control-label text" htmlFor={i}> Bonne réponse </label>
                                </div>

                                <div className=" row col-4 offset-3 mt-1 px-0">

                                    {inputList.length - 1 === i &&
                                    <button className="button-blue-big mx-2" id="add" onClick={handleAddClick}>
                                        <div className="text-button">+ Ajouter une réponse</div>
                                    </button>
                                    }
                                </div>
                            </div>
                        );
                    })}
                </div>

                <div className=" row col-6 offset-3 line"> </div>

                <div className="row col-4 offset-5 mt-4">
                    <ButtonSave/>
                </div>

            </form>


        </div>
            <div className="line-bottom mt-3"> </div>
        </>
    );

}



