import React from "react";
import "../../Assets/Styles/styles.css";


const ButtonDelete = () => {
    return (
        <>
            <button className="button-grey mx-2">
                <div className="text-button">Supprimer</div>
            </button>
        </>
    )
}

export default ButtonDelete;
